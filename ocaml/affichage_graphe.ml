#load "unix.cma";;

(* graphe_to_dot génère la représentation dot d'un graphe orienté ou non avec
les infos issues de l'algo de Dijkstra. fn est le nom du fichier dans lequel on écrit
(recommandé : "/tmp/quelquechose.dot")

   vizgraph fn lance la compilation d'un fichier dot en un PDF puis affiche ce PDF *)


let graphe_to_dot fn oriente graphe marques estimations parents =
  let os = open_out fn in
  let print = output_string os in
  if oriente then print "di";
  print "graph {\n";
  print "rankdir=\"LR\";\n";
  for x = 0 to Array.length graphe - 1 do
    print (string_of_int x);
    print "[label=\"";
    if marques.(x) then print "*";
    print (string_of_int x);
    print " (";
    if estimations.(x) = -1 then
      print "∞"
    else
      print (string_of_int estimations.(x));
    print ")\"];\n";
      List.iter
        (fun (y, w) -> if x < y || oriente then
            begin
              print (string_of_int x);
              if oriente then print " -> " else print "--";
              print (string_of_int y);
              print "[label=\"";
              print (string_of_int w);
              print "\" ";
              if parents.(y) = x || (not oriente && parents.(x) = y) then
                print ",color=\"#FF0000\", penwidth=\"3\"";
              print "];\n"
            end
        )
        graphe.(x)
  done;
  print "}";
  close_out os;;

let run wait cmd =
  if wait then
    ignore (Unix.system cmd)
  else
    ignore (Unix.open_process cmd);;

let rundot fn =
  let bn = try Filename.chop_extension fn with Invalid_argument _ -> fn in
  let pdfn = bn^".pdf" in
  run true ("dot -Tpdf " ^ (Filename.quote fn) ^ " -o " ^ (Filename.quote pdfn));
  pdfn;;

let openPDF wait fn =
    run wait ("evince " ^ (Filename.quote fn));;

let vizgraph wait fn =
  let pdfn = rundot fn in
  openPDF wait pdfn;;
