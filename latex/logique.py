import itertools

def dA(s) :
    return s[0] and s[1] and not s[2]
    
def dB(s) :
    return not s[2]
    
def dC(s) :
    return (s[1] or not s[0]) and not (s[1] and not s[0])
    
def dmA(s) :
    return s[0] == dA(s)
    
def dmB(s) :
    return s[1] == dB(s)
    
def dmC(s) :
    return s[2] == dC(s)
    
def OK(s) :
    return dmA(s) and dmB(s) and dmC(s)
    
def table(nvar, lf) :
    produit = list(itertools.product([False, True], repeat=nvar))
    table = []
    for s in produit :
        ligne = {"s" : s}
        for f in lf :
            ligne[f.__name__] = f(s)
        table.append(ligne)
    return table
    
def table_to_latex(table, vars, lf) :
    fb = lambda b : r"\V" if b else r"\F"
    #keys = [x for x in sorted(list(table[0].keys())) if x!="s"]
    keys = [f.__name__ for f in lf]
    print(r"\begin{array}{"+ "|".join(["c"] * len(vars)) + "||" + "|".join(["c"] * len(keys)) + "}")
    print("&".join(vars + [x for x in keys])+r"\\")
    print(r"\hline")
    for l in table :
        print("&".join([fb(x) for x in l["s"]]+[fb(l[k]) for k in keys]),r"\\", sep="")
    print(r"\end{array}")
        
def table_latex(vars, lf) :
    t = table(len(vars), lf)
    table_to_latex(t, vars, lf)