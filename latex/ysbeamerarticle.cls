\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ysbeamerarticle}
\g@addto@macro\@classoptionslist{,french}
\PassOptionsToClass{french,10pt,a4paper,DIV=18}{scrartcl}
\PassOptionsToClass{french}{beamer}
\RequirePackage{luacode}
\@ifundefined{beamerarticle}{\gdef\beamerarticle{0}\def\CLASSNAME{beamer}}{\ifnum \beamerarticle = 1  \def\CLASSNAME{scrartcl}\else \def\CLASSNAME{beamer}\fi}%
%
\begin{luacode*}
function parseargv()
  local rep = {}
  for k, x in pairs(arg) do
      local kw, vw = string.match(x, "([^=]+)=?([^=]*)")
      rep[kw] = vw
  end
  return rep
end

function string:endswith(suff)
  return suff == "" or string.sub(self, -string.len(suff)) == suff
end

local arguments = parseargv()

local beamer = [[\gdef\beamerarticle{0}\gdef\CLASSNAME{beamer}]]
local scrartcl = [[\gdef\beamerarticle{1}\gdef\CLASSNAME{scrartcl}]]
if arguments["-beamerarticle"] == nil then
  texio.write_nl("-beamerarticle est nil")
  if string.endswith(tex.jobname, "-PRES") then
    tex.print(beamer)
  elseif string.endswith(tex.jobname, "-PRINT") then
    tex.print(scrartcl)
  else
    texio.write_nl("tex.jobname sans suffixe intéressant : "..tex.jobname)
  end
elseif arguments["-beamerarticle"] == "0" then
  tex.print(beamer)
elseif arguments["-beamerarticle"] == "1" then
  tex.print(scrartcl)
else
  texio.write_nl("Erreur : arguments.-beamerarticle vaut "..arguments["-beamerarticle"])
end
\end{luacode*}
\LoadClass{\CLASSNAME}
\ifnum\beamerarticle = 1
\RequirePackage[top=16.5mm,bottom=49.5mm,left=7.5mm,right=50mm]{geometry}
\fi
\endinput
 
