#!/usr/bin/env python3

import subprocess
import sys
import os

def compiler(nf, lsuff, parallel = False) :
   basedir = os.path.dirname(nf)
   builddir = os.path.join(basedir, "_build")
   basejobname = os.path.splitext(os.path.basename(nf))[0]
   if parallel :
      call = subprocess.Popen
   else :
      call = subprocess.call
   # https://stackoverflow.com/a/9745864
   processes = []
   for suff in lsuff :
      options = suff[1]
      processes.append(call('''latexmk -pdflua -shell-escape -bibtex-cond -cd -interaction=batchmode -dvi- -jobname=''' +basejobname+suff[0] +''' -outdir=''' + builddir + ''' -latexoption="--output-directory='''+builddir+''' --synctex=0 ''' + options + '''" ''' + nf, shell=True, stdout=subprocess.PIPE,
                   bufsize=1, close_fds=True,
                   universal_newlines=True))
   if parallel :
      while len(processes) > 0 :
         for p in processes:
               if p.poll() is not None: # process ended
                     p.stdout.close()
                     processes.remove(p)
   for suff in lsuff :
      os.rename(os.path.join(builddir, basejobname+suff[0]+".pdf"), os.path.join(basedir, basejobname+suff[0]+".pdf"))



def clean(nf, suff) :
   subprocess.call(["latexmk", "-cd", "-c",  "-jobname="+os.path.splitext(os.path.basename(nf))[0]+suff, nf])

if sys.argv[1].endswith(".tex") :
   nf = sys.argv[1]
else :
   nf = sys.argv[1]+".tex"

with open(nf) as f :
   for l in f.readlines() :
      if l.strip() == "" or l.strip().startswith("%") :
         continue
      if "beamerarticle" in l or "-PRES" in l or "-PRINT" in l :
         modes = [("-PRES", ""), ("-PRINT", "")]
      elif "beamer" in l :
         modes = [("", "")]
      else :
         modes = [("-ENONCE", ""), ("-PROF", ""), ("-CORRIGE", "")]
      break


compiler(nf, modes, len(sys.argv)>2 and sys.argv[2]=="--parallel")

