\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{CoursTD}
\g@addto@macro\@classoptionslist{,french}
\LoadClass[a4paper,10pt, french, DIV=18]{scrartcl}
\RequirePackage[pass]{geometry}
\def\geometryenonce{\newgeometry{top=16.5mm,bottom=49.5mm,left=7.5mm,right=50mm}}
\RequirePackage{paquets}
\RequirePackage{inputTD}
\endinput