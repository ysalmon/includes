import os

def extraire_caml(nf_tex, nf_ml, nf_texo, tag) :
    output_tex = open(nf_texo, "w")
    output_ml = open(nf_ml, "w")
    incode = False
    nomfonc = ""
    mlref = os.path.basename(nf_ml)
    for l in open(nf_tex) :
        lstrip = l.strip()
        if lstrip == r"\begin{"+tag+"}" :
            incode = True
            nomfonc = ""
            continue
        if incode :
            if nomfonc == "" and "let" in lstrip :
                nomfonc = l[4:]
                if nomfonc.startswith("rec ") :
                    nomfonc = nomfonc[4:]
                nomfonc = nomfonc.split()[0]
                output_ml.write("(*** " + nomfonc + " ***)\n")
            if lstrip == r"\end{"+tag+"}" :
                output_ml.write("(*** " + nomfonc + "_fin ***)\n\n")
                output_tex.write(r"\lstinputcaml[linerange="+nomfonc+"-"+nomfonc+"_fin]{"+ mlref + "}\n")
                incode = False
                continue
            output_ml.write(l)
        else :
            output_tex.write(l)

def caml(s, start=0) :
    try :
        pos_let = s.index("\nlet", start)
        fin = s.index(r";;", pos_let)
        print(s[pos_let:fin+3])
        if s[pos_let+4:].startswith(" rec") :
            delta = 5
        else :
            delta = 0
        pos_nom = s.index(" ", pos_let + delta) + 1
        fin_nom = s.index(" ", pos_nom)
        nom = s[pos_nom:fin_nom]
        print(nom)
        news = s[:pos_let] + "(*** " + nom + " ***)" + s[pos_let:fin+3] + "(*** " + nom + "_fin ***)\n" + s[fin+3:]
        return caml(news, fin + 3 + len("(*** " + nom + " ***)")+ len("(*** " + nom + "_fin ***)\n"))
    except ValueError :
        return s