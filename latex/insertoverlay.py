import re
import sys

p = re.compile("<(?:article:0\|)?((?:[0-9]|\.|,|-)*)>")

def extract(s):
    res = set()
    for spec in p.findall(s) :
        for comma in spec.split(",") :
            res |= set(float(x) for x in comma.split("-") if x)
    return res

def sub(s, newi):
    m = p.search(s)
    if m is None :
        return s
    else :
        return s[:m.start(1)] + ",".join("-".join(str(newi[float(x)]) if x else "" for x in comma.split("-")) for comma in m.group(1).split(","))  + sub(s[m.end(1):], newi)
    return s

def do_file(fn, start=0) :
    ls = open(fn, "r+").readlines()[start:]
    for k in range(len(ls)) :
        if r"\end{frame}" in ls[k] :
            ls = ls[:k]
            break
    xs = set.union(*[extract(l) for l in ls])
    # some integer overlays may not be referred to (especially 1), so we add them
    xs |= set(range(1, int(max(xs))))
    newi = dict(zip(sorted(xs), range(1, len(xs)+1)))
    for l in ls:
        sys.stdout.write(sub(l, newi))

if __name__ == "__main__" :
    if len(sys.argv) > 1 :
        do_file(sys.argv[1], int(sys.argv[2]) if len(sys.argv) > 2 else 0)
    else :
        print("""Give filename as 1st parameter and, optionaly, starting line number as 2nd.
        Program will stop when encountering \\end{frame}.
        Based on https://tex.stackexchange.com/a/139498""")